/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include <QMessageBox>

#include "addtaskdialog.h"
#include "ui_addtaskdialog.h"

#include "constants.h"

using namespace DullPlanner::Widgets;
using namespace DullPlanner::Constants;

AddTaskDialog::AddTaskDialog(StoragePtr storage, std::optional<int> updatingIndex, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::AddTaskDialog),
  _storage(storage),
  _updatingIndex(updatingIndex),
  _model(storage->getModel().toStrongRef())
{
  ui->setupUi(this);
  ui->endDateEdit->setDate(QDate::currentDate());
  ui->startDateEdit->setDate(QDate::currentDate());
  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

  _mapper.setModel( _model.data() );
  _mapper.setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

  // Если менять enum TaskStates или TaskIntervals то все сломается
  // TODO привязка intervalComboBox, stateComboBox к enum
  _mapper.addMapping(ui->nameLineEdit,        _storage->getFieldIndex(tasksNameField        ));
  _mapper.addMapping(ui->groupLineEdit,       _storage->getFieldIndex(tasksGroupField       ));
  _mapper.addMapping(ui->startDateEdit,       _storage->getFieldIndex(tasksStartDateField   ));
  _mapper.addMapping(ui->endDateEdit,         _storage->getFieldIndex(tasksEndDateField     ));
  _mapper.addMapping(ui->intervalComboBox,    _storage->getFieldIndex(tasksIntervalField    ), "currentIndex");
  _mapper.addMapping(ui->stateComboBox,       _storage->getFieldIndex(tasksStateField       ), "currentIndex");
  _mapper.addMapping(ui->closeCheckBox,       _storage->getFieldIndex(tasksIsClosedField    ));
  _mapper.addMapping(ui->descriptionTextEdit, _storage->getFieldIndex(tasksDescriptionField ), "plainText");

  if (_updatingIndex) {
    _mapper.setCurrentIndex( _updatingIndex.value() );
  } else {
    _model->insertRow(_model->rowCount());
    _mapper.toLast();
  }
}

AddTaskDialog::~AddTaskDialog()
{
  delete ui;
}

void AddTaskDialog::accept()
{
  _model->setData(_model->index(_mapper.currentIndex(), _storage->getFieldIndex(tasksUpdateDateField)),
                  QDate::currentDate());
  _mapper.submit();
  QDialog::accept();
}

void AddTaskDialog::reject()
{
  _mapper.revert();

  if (!_updatingIndex) {
    _model->removeRow(_model->rowCount() - 1);
  }

  QDialog::reject();
}

void AddTaskDialog::on_startDateEdit_userDateChanged(const QDate &date)
{
  if (date > ui->endDateEdit->date()) {
    QMessageBox::warning(this, "Некорректная дата", "Дата начала задачи позже даты завершения");
  }
}

void AddTaskDialog::on_endDateEdit_userDateChanged(const QDate &date)
{
  if (date < ui->startDateEdit->date()) {
    QMessageBox::warning(this, "Некорректная дата", "Дата завершения задачи раньше даты начала");
  }
}
