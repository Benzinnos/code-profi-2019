/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include "quickpanel.h"
#include "ui_quickpanel.h"
#include "constants.h"
//#include "quickpaneldelegate.h"

#include <QDebug>
#include <QMessageBox>

using namespace DullPlanner::Widgets;
using namespace DullPlanner::Constants;

QuickPanel::QuickPanel(StoragePtr storage, QWidget *parent) :
  QWidget(parent),
  ui(new Ui::QuickPanel),
  _storage(storage),
  _model(storage)
{
  ui->setupUi(this);
  setWindowFlags(Qt::WindowMinimizeButtonHint | Qt::WindowStaysOnTopHint);

  _model.setSourceModel(_storage->getModel().toStrongRef().data());

  ui->taskListView->setModel(&_model);
//  ui->taskListView->setItemDelegate(new QuickPanelDelegate());

  _tray.setIcon(QIcon("qrc:/img/img/play-29952.ico"));
  _tray.show();

  connect(&_taskSpentTimer, &QTimer::timeout, this, &QuickPanel::onTimerTimeout);
  _taskSpentTimer.setInterval(/*writeSpentTimeTimeout **/ 100);
  _taskSpentTimer.start();
}

QuickPanel::~QuickPanel()
{
  delete ui;
}

void QuickPanel::onTimerTimeout()
{
  auto sourceModel = _model.sourceModel();

  // Подсчет не точный
  for (auto i = 0; i < _model.rowCount(); ++i) {
    auto sourceIndex = _model.mapToSource(_model.index(i, 0));

    auto m_spentTime = sourceModel->data(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksSpentTimeField)))
                         .toULongLong();
    auto m_endDate   = sourceModel->data(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksEndDateField)))
                         .toDate();
    auto m_name      = sourceModel->data(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksNameField)))
                         .toString();

    sourceModel->setData(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksSpentTimeField)), m_spentTime + 1);

    if (QDate::currentDate().addDays(1) == m_endDate) {
      if (!_notyficated.contains(m_name)) {
        _tray.showMessage(tr("Планировщик задач"), tr("Заканчивается задача %1").arg(m_name));
        QMessageBox::information(this, tr("Планировщик задач"), tr("Заканчивается задача %1").arg(m_name));
        _notyficated.insert(m_name);
      }

    }

    if (QDate::currentDate().addDays(1) > m_endDate) {
      _tray.showMessage(tr("Планировщик задач"), tr("Просрочена задача %1").arg(m_name));
      QMessageBox::information(this, tr("Планировщик задач"), tr("Просрочена задача %1").arg(m_name));
      sourceModel->setData(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksStateField)), TaskStates::overdue);
    }

    sourceModel->setData(sourceModel->index(sourceIndex.row(), _storage->getFieldIndex(tasksUpdateDateField)), QDate::currentDate());
  }

  sourceModel->submit();
}
