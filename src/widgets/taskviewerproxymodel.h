/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef TASKVIEWERPROXYMODEL_H
#define TASKVIEWERPROXYMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include <QDate>

namespace DullPlanner {
namespace Widgets {

/*!
 * \brief The TaskViewerProxyModel class Модель для выборки по единственной дате
 */
class TaskViewerProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT
public:
  /*!
   * \brief TaskViewerProxyModel
   * \param dateFieldIndex Индекс поля с датой
   * \param parent Указатель на родительский класс
   */
  explicit TaskViewerProxyModel(int dateFieldIndex, QObject *parent = nullptr);
  virtual ~TaskViewerProxyModel() override = default;

  /*!
   * \brief filterDate Возвращает дату для фильтрации
   * \return Дата для фильтрации
   */
  QDate filterDate() const { return _date; }

  /*!
   * \brief setFilterDate Установка даты для фильтрации
   * \param date Дата для фильтрации
   */
  void setFilterDate(const QDate &date) { beginResetModel(); _date = date; endResetModel(); }

signals:

  // QSortFilterProxyModel interface
protected:
  virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

public slots:

private:
  QDate _date;
  int   _dateFieldIndex;
};

} // namespace Widgets
} // namespace DullPlanner

#endif // TASKVIEWERPROXYMODEL_H
