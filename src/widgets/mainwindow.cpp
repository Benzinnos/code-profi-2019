/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include <QCloseEvent>
#include <QTextCharFormat>

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "addtaskdialog.h"
#include "constants.h"

using namespace DullPlanner::Widgets;
using namespace DullPlanner::Constants;

MainWindow::MainWindow(StoragePtr storage, QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  _storage(storage),
  _calendarModel(storage->getFieldIndex(tasksStartDateField),
                 storage->getFieldIndex(tasksEndDateField)),
  _taskModel(storage->getFieldIndex(tasksStartDateField))
{
  ui->setupUi(this);

  _calendarModel.setSourceModel(_storage->getModel().toStrongRef().data());
  _taskModel.setSourceModel(_storage->getModel().toStrongRef().data());

  ui->taskList->setModel(&_taskModel);
  ui->taskList->setModelColumn(storage->getFieldIndex(tasksNameField));
  ui->taskCalendar->clicked(QDate::currentDate());

  ui->selectedDateLabel->setText(QDate::currentDate().toString());
  ui->taskCalendar->showToday();
  ui->taskCalendar->currentPageChanged(QDate::currentDate().year(), QDate::currentDate().month());
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
  emit mainWindowClosing();
  event->accept();
}

void MainWindow::on_addTaskAction_triggered()
{
  AddTaskDialog d(_storage, /* updatingIndex = */std::nullopt, this);
  d.exec();
}

void MainWindow::on_searchTaskAction_triggered()
{

}

void MainWindow::on_exitAction_triggered()
{
  emit mainWindowClosing();
}

void MainWindow::on_taskCalendar_clicked(const QDate &date)
{
  ui->selectedDateLabel->setText(date.toString());
  _taskModel.setFilterDate(date);
}

void MainWindow::on_taskCalendar_currentPageChanged(int year, int month)
{
  _calendarModel.setFilterMaximumDate(QDate(year, month, 1).addMonths(1));
  _calendarModel.setFilterMinimumDate(QDate(year, month, 1));

  QTextCharFormat tf;
  tf.setBackground(Qt::green);

  for (auto i = 0; i < _calendarModel.rowCount(); ++i) {
    auto startDate = _calendarModel.data(_calendarModel.index(
                                           i, _storage->getFieldIndex(tasksStartDateField))
                                         ).toDate();
    ui->taskCalendar->setDateTextFormat(startDate, tf);
  }

}

void MainWindow::on_taskList_doubleClicked(const QModelIndex &index)
{
  AddTaskDialog d(_storage, _taskModel.mapToSource(index).row(), this);

  if (d.exec() == QDialog::Accepted) {
    _calendarModel.resetModel();
  };
}
