/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include "quickpanelproxymodel.h"
#include "types.h"
#include "constants.h"

using namespace DullPlanner;
using namespace DullPlanner::Widgets;
using namespace DullPlanner::Constants;

QuickPanelProxyModel::QuickPanelProxyModel(StoragePtr storage, QObject *parent) :
  QSortFilterProxyModel(parent),
  _storage(storage)
{

}

bool QuickPanelProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
  QModelIndex index  = sourceModel()->index(sourceRow, _storage->getFieldIndex(tasksStateField),    sourceParent);
  QModelIndex indexc = sourceModel()->index(sourceRow, _storage->getFieldIndex(tasksIsClosedField), sourceParent);

  if (sourceModel()->data(indexc).toBool()) {
    return false;
  }

  return sourceModel()->data(index).toInt() == TaskStates::running ||
      sourceModel()->data(index).toInt() == TaskStates::paused;
}

QVariant QuickPanelProxyModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == Qt::DisplayRole) {
    return QString("Задача %1. Статус: %2. Окончание: %3")
      .arg(sourceModel()->data(
                          sourceModel()->index(mapToSource(index).row(), _storage->getFieldIndex(tasksNameField) )).toString())
      .arg(sourceModel()->data(
                          sourceModel()->index(mapToSource(index).row(), _storage->getFieldIndex(tasksStateField) )).toInt() ==
               TaskStates::paused ? "Пауза" : "В работе")
      .arg((sourceModel()->data(
                           sourceModel()->index(mapToSource(index).row(),
                                                _storage->getFieldIndex(tasksEndDateField) )).toDate().toString()));

  } else {
    return QVariant();
  }
}
