/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef QUICKPANELDELEGATE_H
#define QUICKPANELDELEGATE_H

#include <QWidget>
#include <QStyledItemDelegate>
#include <QScopedPointer>

namespace Ui {
class QuickPanelDelegate;
}

namespace DullPlanner {
namespace Widgets {

class QuickPanelDelegateWidget : public QWidget
{
  Q_OBJECT

public:
  explicit QuickPanelDelegateWidget(QWidget *parent = nullptr);
  ~QuickPanelDelegateWidget();

private:
  Ui::QuickPanelDelegate *ui;
};


class QuickPanelDelegate : public QStyledItemDelegate
{
  Q_OBJECT

public:
  explicit QuickPanelDelegate(QStyledItemDelegate *parent = nullptr);
  virtual ~QuickPanelDelegate() override = default;

  QScopedPointer<QuickPanelDelegateWidget> m_wgt;

  // QAbstractItemDelegate interface
public:
  virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

} // namespace Widgets
} // namespace DullPlanner

#endif // QUICKPANELDELEGATE_H
