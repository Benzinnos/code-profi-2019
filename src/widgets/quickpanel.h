/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef QUICKPANEL_H
#define QUICKPANEL_H

#include <QWidget>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QSet>

#include "quickpanelproxymodel.h"
#include "storage.h"

namespace Ui {
class QuickPanel;
}

namespace DullPlanner {
namespace Widgets {

class QuickPanel : public QWidget
{
  Q_OBJECT

public:
  explicit QuickPanel(StoragePtr storage, QWidget *parent = nullptr);
  ~QuickPanel();

protected slots:
  void onTimerTimeout();

private:
  Ui::QuickPanel *ui;
  StoragePtr           _storage;
  QuickPanelProxyModel _model;
  QTimer               _taskSpentTimer;
  QSystemTrayIcon      _tray;
  QSet<QString>        _notyficated;

};

} // namespace Widgets
} // namespace DullPlanner

#endif // QUICKPANEL_H
