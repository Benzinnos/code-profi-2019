/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef TASKCALENDARPROXYMODEL_H
#define TASKCALENDARPROXYMODEL_H

#include <QObject>
#include <QDate>
#include <QSortFilterProxyModel>

namespace DullPlanner {
namespace Widgets {

/*!
 * \brief The DateFilterProxyModel class Модель для фильтрации по дате
 */
class TaskCalendarProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  /*!
   * \brief DateFilterProxyModel
   * \param dateFieldIndex Индекс по которому будет происходить фильтрация
   * \param parent Указатель на родителя
   */
  explicit TaskCalendarProxyModel(int startDateFieldIndex, int endDateFieldIndex, QObject *parent = nullptr);
  virtual ~TaskCalendarProxyModel() override = default;

  /*!
   * \brief filterMinimumDate Нижняя граница даты для фильтрации
   * \return Дата
   */
  QDate filterMinimumDate() const { return _minDate; }
  /*!
   * \brief setFilterMinimumDate Установка нижней граница даты для фильтрации
   * \param date Дата
   */
  void setFilterMinimumDate(const QDate &date) { beginResetModel(); _minDate = date; endResetModel(); }

  /*!
   * \brief filterMaximumDate Установка верхняя граница даты для фильтрации
   * \return Дата
   */
  QDate filterMaximumDate() const { return _maxDate; }
  /*!
   * \brief setFilterMaximumDate Установка верхней границы даты для фильтрации
   * \param date Дата
   */
  void setFilterMaximumDate(const QDate &date) { beginResetModel(); _maxDate = date; endResetModel(); }

  /*!
   * \brief resetModel Ресет модели
   */
  void resetModel() { beginResetModel(); endResetModel(); }

  // QSortFilterProxyModel interface
protected:
  virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
  QDate _minDate;
  QDate _maxDate;
  int   _startDateFieldIndex;
  int   _endDateFieldIndex;

  bool _dateInRange(const QDate &date) const;
};

} // namespace Widgets
} // namespace DullPlanner

#endif // TASKCALENDARPROXYMODEL_H
