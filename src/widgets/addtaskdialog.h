/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef ADDTASKDIALOG_H
#define ADDTASKDIALOG_H

#include <QDialog>
#include <QSharedPointer>
#include <QDataWidgetMapper>

#include <optional>

#include "storage.h"


namespace Ui {
class AddTaskDialog;
}

namespace DullPlanner {
namespace Widgets {

/*!
 * \brief The AddTaskDialog class Диалог редактирования задачи
 */
class AddTaskDialog : public QDialog
{
  Q_OBJECT

public:
  /*!
   * \brief AddTaskDialog
   * \details При создании захватывает модель хранилища,
   * реинициализация хранилища при открытом диалоге приведет к неопределенному поведению
   * \param storage Обертка над хранилищем данных
   * \param updatingIndex idx - изменить существующий, std::nullopt - создать новый элемент,
   * \param parent Указатель на родитель
   */
  explicit AddTaskDialog(StoragePtr storage, std::optional<int> updatingIndex, QWidget *parent = nullptr);
  virtual ~AddTaskDialog() override;

public slots:
  /*!
   * \brief accept Декоратор метода
   * \details При нажатии, данные отправляются в модель
   */
  virtual void accept() override;

  /*!
   * \brief reject Декоратор метода
   * \details При нажатии, данные не отправляются в модель
   */
  virtual void reject() override;

private slots:
  void on_startDateEdit_userDateChanged(const QDate &date);
  void on_endDateEdit_userDateChanged(const QDate &date);

private:
  Ui::AddTaskDialog *ui;
  StoragePtr _storage;
  std::optional<int> _updatingIndex;
  QSharedPointer<QAbstractItemModel> _model;
  QDataWidgetMapper _mapper;
};

} // namespace Widgets
} // namespace DullPlanner

#endif // ADDTASKDIALOG_H
