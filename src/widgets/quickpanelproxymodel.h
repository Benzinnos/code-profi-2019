/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef QUICKPANELPROXYMODEL_H
#define QUICKPANELPROXYMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>

#include "storage.h"

namespace DullPlanner {
namespace Widgets {

/*!
 * \brief The QuickPanelProxyModel class Модель отображения текущих выполняемых задач
 */
class QuickPanelProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT

public:
  explicit QuickPanelProxyModel(StoragePtr storage, QObject *parent = nullptr);
  virtual ~QuickPanelProxyModel() override = default;

signals:

public slots:

  // QSortFilterProxyModel interface
protected:
  virtual bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;

private:
  StoragePtr _storage;

  // QAbstractItemModel interface
public:
  virtual QVariant data(const QModelIndex &index, int role) const override;
};


} // namespace Widgets
} // namespace DullPlanner

#endif // QUICKPANELPROXYMODEL_H
