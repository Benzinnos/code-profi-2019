/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include "quickpaneldelegate.h"
#include "ui_quickpaneldelegate.h"

#include <QPainter>

using namespace DullPlanner::Widgets;

QuickPanelDelegateWidget::QuickPanelDelegateWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::QuickPanelDelegate)
{
  ui->setupUi(this);
}

QuickPanelDelegateWidget::~QuickPanelDelegateWidget()
{
  delete ui;
}

QuickPanelDelegate::QuickPanelDelegate(QStyledItemDelegate *parent) :
  QStyledItemDelegate(parent),
  m_wgt(new QuickPanelDelegateWidget())
{

}

void QuickPanelDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &) const
{
  m_wgt->resize(option.rect.size());
  painter->save();
  painter->translate(option.rect.topLeft());
  m_wgt->render(painter);
  painter->restore();
  m_wgt->render(painter);
}
