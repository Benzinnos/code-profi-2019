/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <QSharedPointer>
#include <QWeakPointer>
#include <QDataWidgetMapper>
#include <QSqlDatabase>
#include <QAbstractItemModel>

#include "types.h"

class QSqlTableModel;

namespace DullPlanner {

using StorageMapper = QWeakPointer<QDataWidgetMapper>;
using StorageModel  = QWeakPointer<QAbstractItemModel>;

/*!
 * \brief The Storage class Обертка над хранилищем данных о задачах
 */
class Storage : public QObject
{
  Q_OBJECT

public:
  explicit Storage(QObject *parent = nullptr);
  virtual ~Storage() = default;

  /*!
   * \brief initialize Lazy constructor
   * \return Информация об ошибке
   */
  virtual ErrorInfo initialize();

  /*!
   * \brief initialize Overload lazy constructor
   * \param path Путь до файла хранилища
   * \return Информация об ошибке
   */
  virtual ErrorInfo initialize(const QString &path);

  /*!
   * \brief getMapper Получить маппер хранилища
   * \return Возвращает указаель на адаптер хранилища для виджетов
   */
  StorageMapper getMapper() const {
    return m_mapper.toWeakRef();
  }

  /*!
   * \brief getModel Получить модель хранилища
   * \return Возвращает указаель на модель хранилища
   */
  StorageModel getModel() const {
    return m_model.toWeakRef();
  }

  /*!
   * \brief getFieldIndex Возвращает индекс поля
   * \param name Имя поля
   * \return Если поля не существует, возвращает -1
   */
  virtual int getFieldIndex(const QString &name) const;

public slots:
  /*!
   * \brief load Загрузка данных из хранилища
   * \param[out] tasks Данные из хранилища
   * \return Информация об ошибке
   */
  virtual ErrorInfo load(TaskList &tasks) const;

  /*!
   * \brief load Загрузка данных из хранилища
   * \param[out] tasks Данные из хранилища
   * \param filter Загружаемых выгружаемых данных
   * \return Информация об ошибке
   */
  virtual ErrorInfo load(TaskList &tasks, const QString &filter) const;

  /*!
   * \brief store Выгрузка данных в хранилище
   * \param tasks Выгружаемые данные
   * \return Информация об ошибке
   */
  virtual ErrorInfo store(const TaskList &tasks);

signals:
  /*!
   * \brief dataChanged Сигнал при изменении данных в хранилище
   */
  void dataChanged();

protected:
  QSharedPointer<QDataWidgetMapper>  m_mapper;
  QSharedPointer<QAbstractItemModel> m_model;
  QSqlDatabase  m_db;

private:
  bool _createTaskTable();
  void _initializeModel();
  void _initializeMapper();
  QString _queryErrorToString(const QSqlQuery &query) const;
  std::variant<bool, QString> _checkTaskExists(const Task &task) const;

  QSharedPointer<QSqlTableModel> _internalModel;

};
using StoragePtr = QSharedPointer<Storage>;

} // namespace DullPlanner

#endif // STORAGE_H
