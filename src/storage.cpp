/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include <QApplication>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlTableModel>

#include <variant>

#include "storage.h"
#include "constants.h"

using namespace DullPlanner;
using namespace DullPlanner::Constants;

Storage::Storage(QObject *parent) :
  QObject(parent)
{

}

ErrorInfo Storage::initialize() {
  return initialize(QApplication::applicationDirPath() + Constants::defaultDatabasePath);
}

ErrorInfo Storage::initialize(const QString &path)
{
  try {

    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(path);

    // Соединение с БД
    if (!m_db.open()) {
      throw tr("Не удалось инициализировать хранилище. Путь: %1").arg(path);
    }

    // Создание основной рабочей таблицы
    if (!_createTaskTable()) {
      throw tr("Не создать таблицу %1 в хранилище.").arg(tasksTableName);
    }

    _initializeModel();

  } catch (const QString &error) {
    m_db.close();
    return ErrorInfo(error);
  }

  connect(m_model.data(), &QAbstractItemModel::dataChanged,
          [this](const QModelIndex &, const QModelIndex &) {
            emit dataChanged();
          });

  return ErrorInfo(std::nullopt);
}

int Storage::getFieldIndex(const QString &name) const
{
  return _internalModel->fieldIndex(name);
}

ErrorInfo Storage::load(TaskList &tasks) const
{
  return load(tasks, "");
}

ErrorInfo Storage::load(TaskList &tasks, const QString &filter) const
{
  QSqlQuery query(m_db);
  tasks.clear();

  if (!query.exec(QString("select * from %1 %2")
                    .arg(tasksTableName)
                    .arg( filter.isEmpty() ? "" : (QString("where ") + filter)) )) {
    return QString("Не удалось загрузить задания. %1")
      .arg(_queryErrorToString(query));
  }

  while (query.next()) {
    tasks.push_back({
      query.value( tasksIdField        ).toInt(),
      query.value( tasksSpentTimeField ).toInt(),
      static_cast<TaskStates>(    query.value( tasksStateField    ).toInt()),
      static_cast<TaskIntervals>( query.value( tasksIntervalField ).toInt()),
      query.value( tasksNameField        ).toString(),
      query.value( tasksDescriptionField ).toString(),
      query.value( tasksGroupField       ).toString(),
      query.value( tasksMiscField        ).toString(),
      query.value( tasksStartDateField   ).toDate(),
      query.value( tasksEndDateField     ).toDate(),
      query.value( tasksUpdateDateField  ).toDate(),
      query.value( tasksIsClosedField    ).toBool(),
    });
  }

  return ErrorInfo(std::nullopt);
}

ErrorInfo Storage::store(const TaskList &tasks)
{ 
  QSqlQuery insertQuery(m_db);
  insertQuery.prepare(QString("insert into %1 (%2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12) "
                              "values (:%2, :%3, :%4, :%5, :%6, :%7, :%8, :%9, :%10, :%11, :%12)")
                        .arg( tasksTableName        )
                        .arg( tasksStateField       )
                        .arg( tasksIntervalField    )
                        .arg( tasksNameField        )
                        .arg( tasksDescriptionField )
                        .arg( tasksGroupField       )
                        .arg( tasksMiscField        )
                        .arg( tasksStartDateField   )
                        .arg( tasksEndDateField     )
                        .arg( tasksSpentTimeField   )
                        .arg( tasksIsClosedField    )
                        .arg( tasksUpdateDateField  ));
  QSqlQuery updateQuery(m_db);
  updateQuery.prepare(QString("update %1 set %2=:%2, %3=:%3, %4=:%4, %5=:%5, %6=:%6, "
                              "%7=:%7, %8=:%8, %9=:%9, %10=:%10, %11=:%11, %12=:%12 where %13=:%13")
                        .arg( tasksTableName        )
                        .arg( tasksStateField       )
                        .arg( tasksIntervalField    )
                        .arg( tasksNameField        )
                        .arg( tasksDescriptionField )
                        .arg( tasksGroupField       )
                        .arg( tasksMiscField        )
                        .arg( tasksStartDateField   )
                        .arg( tasksEndDateField     )
                        .arg( tasksSpentTimeField   )
                        .arg( tasksIsClosedField    )
                        .arg( tasksUpdateDateField  )
                        .arg( tasksIdField          ));

  for (auto &&task : tasks) {
    bool isNeedUpdate = false; // update or insert new

    if (task.m_id) {
      if (auto result = _checkTaskExists(task); std::holds_alternative<QString>(result)) {
        return ErrorInfo( std::get<QString>(result) );
      } else if ( std::get<bool>(result) ) {
        isNeedUpdate = true;
      }
    }

    // Currently SQlite UPSERT is not supported
    QSqlQuery query = isNeedUpdate ? updateQuery : insertQuery;

    query.bindValue(QString(":%1").arg( tasksStateField       ), task.m_state       );
    query.bindValue(QString(":%1").arg( tasksIntervalField    ), task.m_interval    );
    query.bindValue(QString(":%1").arg( tasksNameField        ), task.m_name        );
    query.bindValue(QString(":%1").arg( tasksDescriptionField ), task.m_description );
    query.bindValue(QString(":%1").arg( tasksGroupField       ), task.m_group       );
    query.bindValue(QString(":%1").arg( tasksMiscField        ), task.m_misc        );
    query.bindValue(QString(":%1").arg( tasksStartDateField   ), task.m_startDate   ); // "yyyy-MM-dd"
    query.bindValue(QString(":%1").arg( tasksEndDateField     ), task.m_endDate     );
    query.bindValue(QString(":%1").arg( tasksSpentTimeField   ), task.m_spentTime   );
    query.bindValue(QString(":%1").arg( tasksIsClosedField    ), task.m_isClosed    );
    query.bindValue(QString(":%1").arg( tasksUpdateDateField  ), task.m_updateDate  );

    if (isNeedUpdate) {
      query.bindValue(QString(":%1").arg(tasksIdField), task.m_id);
    }

    if ( !query.exec() ) {
      return QString("Не удалось записать элемент. %1")
        .arg(_queryErrorToString(query));
    }
  }

  // Updateing model and proxy models
  if (_internalModel) {
    _internalModel->select();
  }
  emit dataChanged();

  return ErrorInfo(std::nullopt);
}

bool Storage::_createTaskTable()
{
  QSqlQuery query(m_db);
  return query.exec(QString("create table if not exists %1 (%2 integer primary key, "
                            "%3 int, %4 int, %5 text, %6 text, %7 text, "
                            "%8 text, %9 date, %10 date, %11 int default 0, %12 boolean, %13 date)")
                      .arg( tasksTableName        )
                      .arg( tasksIdField          )
                      .arg( tasksStateField       )
                      .arg( tasksIntervalField    )
                      .arg( tasksNameField        )
                      .arg( tasksDescriptionField )
                      .arg( tasksGroupField       )
                      .arg( tasksMiscField        )
                      .arg( tasksStartDateField   )
                      .arg( tasksEndDateField     )
                      .arg( tasksSpentTimeField   )
                      .arg( tasksIsClosedField    )
                      .arg( tasksUpdateDateField  ));
}

void Storage::_initializeModel()
{
  QScopedPointer<QSqlTableModel> sqlModel( new QSqlTableModel(nullptr, m_db) );
  sqlModel->setTable(tasksTableName);
  sqlModel->select();
  _internalModel.reset(sqlModel.data(), &QObject::deleteLater);
  m_model.reset(sqlModel.take(), &QObject::deleteLater);
}

void Storage::_initializeMapper()
{
  m_mapper.reset(new QDataWidgetMapper(), &QObject::deleteLater);
  m_mapper->setModel(m_model.data());
}

QString Storage::_queryErrorToString(const QSqlQuery &query) const
{
  return tr("Ошибка запроса. %1: %2")
    .arg(query.lastQuery())
    .arg(query.lastError().text());
}

std::variant<bool, QString> Storage::_checkTaskExists(const Task &task) const
{
  QSqlQuery query(m_db);

  if ( !query.exec(QString("select exists(select 1 from %1 where %2=%3)")
                    .arg(tasksTableName)
                    .arg(tasksIdField)
                    .arg(task.m_id)) ) {
    return _queryErrorToString(query);
  }

  while (query.next()) {
    return query.value(0).toBool();
  }

  return tr("Ответ не должен быть пустым");
}
