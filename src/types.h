/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef TYPES_H
#define TYPES_H

#include <QObject>
#include <QString>
#include <QDate>
#include <QList>

#include <optional>
#include <tuple>

namespace DullPlanner {

/*!
 * \brief The TaskStates enum Состояния задачи
 */
enum TaskStates : int {
  notStared,  /**< Не запускалась */
  running,    /**< Выполняется */
  paused,     /**< Приостановлена */
  overdue,    /**< Просрочена */
};

/*!
 * \brief The TaskIntervals enum Периодичность выполнения задачи
 */
enum TaskIntervals : int {
  single,   /**< Однократно */
  daily,    /**< Каждый день */
  weekly,   /**< Каждую неделю */
  mounthly, /**< Каждый месяц */
  yearly,   /**< Каждый год */
};

/*!
 * \brief The Task struct Информация о задаче
 */
struct Task
{
  qint64        m_id         = 0;                     /**< Идентификатор задачи */
  qint64        m_spentTime  = 0;                     /**< Затраченное время на задачу, минуты */
  TaskStates    m_state      = TaskStates::notStared; /**< Состояние задачи */
  TaskIntervals m_interval   = TaskIntervals::single; /**< Периодичность выполнения */
  QString       m_name;                               /**< Название задачи */
  QString       m_description;                        /**< Описание задачи */
  QString       m_group;                              /**< Тема задачи (группа) */
  QString       m_misc;                               /**< Прочая информация */
  QDate         m_startDate  = QDate::currentDate();  /**< Дата начала выполнения задачи */
  QDate         m_endDate    = QDate::currentDate();  /**< Дата завершения выполнения задачи */
  QDate         m_updateDate = QDate::currentDate();  /**< Дата обновления выполнения задачи */
  bool          m_isClosed   = false;                 /**< Признак завершения задачи */

  // hidden friends operators
private:
  friend auto taskToTuple(const Task& p){
    return std::make_tuple(p.m_id,       p.m_spentTime,  p.m_state,
                           p.m_interval, p.m_name,       p.m_description,
                           p.m_group,    p.m_misc,       p.m_startDate,
                           p.m_endDate,  p.m_updateDate, p.m_isClosed);
  }

  friend bool operator==(const Task& lhs, const Task& rhs){
    return taskToTuple(lhs) == taskToTuple(rhs);
  }
};
using TaskList = QList<Task>;

/*!
 * \brief The Task struct Информация об ошибке
 */
using ErrorInfo = std::optional<QString>;

} // namespace DullPlanner

Q_DECLARE_METATYPE(DullPlanner::Task)
Q_DECLARE_METATYPE(DullPlanner::TaskList)

#endif // TYPES_H
