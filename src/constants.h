/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace DullPlanner {
namespace Constants {

constexpr char defaultDatabasePath[] = "/tasks.db"; /**< Путь до файла с базой данных по-умолчанию */
constexpr char defaultsettingsPath[] = "";          /**< Путь до файла настроек по-умолчанию */

constexpr char tasksTableName[]        = "tasks";       /**< Имя таблицы с задачами */
constexpr char tasksIdField[]          = "id";          /**< Имя поля Идентификатор задачи */
constexpr char tasksStateField[]       = "state";       /**< Имя поля Состояние задачи */
constexpr char tasksIntervalField[]    = "interval";    /**< Имя поля Периодичность выполнения */
constexpr char tasksNameField[]        = "name";        /**< Имя поля Название задачи */
constexpr char tasksDescriptionField[] = "description"; /**< Имя поля Описание задачи */
constexpr char tasksGroupField[]       = "tgroup";      /**< Имя поля Тема задачи (группа) */
constexpr char tasksMiscField[]        = "misc";        /**< Имя поля Прочая информация */
constexpr char tasksStartDateField[]   = "start_date";  /**< Имя поля Дата и время начала выполнения задачи */
constexpr char tasksEndDateField[]     = "end_date";    /**< Имя поля Дата и время завершения выполнения задачи */
constexpr char tasksUpdateDateField[]  = "update_date"; /**< Имя поля Дата обновления выполнения задачи */
constexpr char tasksSpentTimeField[]   = "spent_time";  /**< Имя поля Затраченное время на задачу */
constexpr char tasksIsClosedField[]    = "is_closed";   /**< Имя поля Признак завершения задачи */

constexpr int writeSpentTimeTimeout    = 60;            /**< Таймаут записи потраченого времени на задачу (сек) */

} // namespace Constants
} // namespace DullPlanner

#endif // CONSTANTS_H
