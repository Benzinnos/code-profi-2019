/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include <QApplication>
#include <QObject>
#include <QSharedPointer>
#include <QMessageBox>

#include "widgets/mainwindow.h"
#include "widgets/quickpanel.h"

#include "storage.h"

using DullPlanner::Widgets::MainWindow;
using DullPlanner::Widgets::QuickPanel;
using DullPlanner::StoragePtr;
using DullPlanner::Storage;

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  StoragePtr _st(new Storage(), &QObject::deleteLater);
  if (auto r = _st->initialize(); r) {
    QMessageBox::critical(nullptr, QObject::tr("Ошибка инициализации хранилища"), r.value());
    return -1;
  }

  MainWindow w(_st);
  QuickPanel panel(_st);
  panel.move(w.pos().x() + 200, w.pos().y() + 100);

  w.show();
  panel.show();

  // closing quick panel when closing main window
  QObject::connect(&w, &MainWindow::mainWindowClosing, &panel, &QuickPanel::close);
  QObject::connect(&w, &MainWindow::mainWindowClosing, &a,     &QApplication::quit);

  return a.exec();
}
