QT += testlib
QT += gui sql
CONFIG += qt warn_on depend_includepath testcase

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

CONFIG += c++17

INCLUDEPATH += \
    ../src

HEADERS += \
    ../src/constants.h \
    ../src/types.h \
    ../src/storage.h

SOURCES +=  \
    ../src/storage.cpp \
    tst_storage.cpp
