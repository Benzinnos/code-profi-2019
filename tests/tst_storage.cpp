/*
** Copyright 2019 NONAME CONTEST PARTICIPANT
**
** Licensed under the MIT license:
** http://www.opensource.org/licenses/mit-license.php
**
** Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
** documentation files (the "Software"), to deal in the Software without restriction, including without limitation
** the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and
** to permit persons to whom the Software is furnished to do so, subject to the following conditions:
**
** The above copyright notice and this permission notice shall be included in all copies or substantial portions of
** the Software.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
** THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
** AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
** CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
** DEALINGS IN THE SOFTWARE.
*/

#include <QtTest>
#include <QCoreApplication>
#include <QFile>
#include <QScopedPointer>

#include "types.h"
#include "storage.h"
#include "constants.h"

using namespace DullPlanner;
using namespace DullPlanner::Constants;

class Tst_Stroage : public QObject
{
  Q_OBJECT

public:
  Tst_Stroage();
  ~Tst_Stroage();

private slots:
  void initTestCase();
  void cleanupTestCase();
  void test_insert();
  void test_update();
  void tst_loadFiltred();

private:
  QScopedPointer<Storage> _st;
  QString _dbPath;
  Task _tstTask;

};

Tst_Stroage::Tst_Stroage() :
  _dbPath(QCoreApplication::applicationDirPath() + "/test.db")
{

}

Tst_Stroage::~Tst_Stroage()
{

}

void Tst_Stroage::initTestCase()
{
  _st.reset(new Storage());
  QCOMPARE(_st->initialize(_dbPath), std::nullopt);

  _tstTask.m_id          = 1;
  _tstTask.m_name        = "test";
  _tstTask.m_description = "description";
  _tstTask.m_group       = "group";
  _tstTask.m_misc        = "misc";
  _tstTask.m_spentTime   = 10;
  _tstTask.m_state       = TaskStates::overdue;
  _tstTask.m_interval    = TaskIntervals::weekly;
  _tstTask.m_startDate   = QDate::currentDate();
  _tstTask.m_endDate     = QDate::currentDate().addDays(1);
  _tstTask.m_isClosed    = true;
  _tstTask.m_updateDate  = QDate::currentDate();
}

void Tst_Stroage::cleanupTestCase()
{
  _st.reset(nullptr);
  QFile d(_dbPath);
  QVERIFY(d.open(QIODevice::WriteOnly | QIODevice::Truncate));
}

void Tst_Stroage::test_insert()
{
  QCOMPARE(_st->store({ _tstTask }), std::nullopt);

  //read
  TaskList readedTasks;
  QCOMPARE(_st->load(readedTasks), std::nullopt);
  QCOMPARE(readedTasks.size(), 1);

  auto actual = readedTasks.at(0);
  QCOMPARE(actual, _tstTask);
}

void Tst_Stroage::test_update()
{
  QCOMPARE(_st->store({ _tstTask }), std::nullopt);
  _tstTask.m_isClosed = false;
  QCOMPARE(_st->store({ _tstTask }), std::nullopt);

  // read
  TaskList readedTasks;
  QCOMPARE(_st->load(readedTasks), std::nullopt);
  QCOMPARE(readedTasks.size(), 1);

  auto actual = readedTasks.at(0);
  QCOMPARE(actual, _tstTask);
}

void Tst_Stroage::tst_loadFiltred()
{
  QCOMPARE(_st->store({ _tstTask }), std::nullopt);
  _tstTask.m_id += 1;
  QCOMPARE(_st->store({ _tstTask }), std::nullopt);

  const auto readingId = 2;
  const auto filter = QString("%1=%2")
                        .arg(tasksIdField)
                        .arg(readingId);
  TaskList readedTasks;
  QCOMPARE(_st->load(readedTasks, filter), std::nullopt);
  QCOMPARE(readedTasks.size(), 1);
  QCOMPARE(readedTasks.at(0).m_id, readingId);
}

QTEST_MAIN(Tst_Stroage)

#include "tst_storage.moc"
